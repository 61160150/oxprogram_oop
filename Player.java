package myXOgame;
public class Player {

	private char name;
	private int win;
	private int lose;
	private int draw;


	public Player(char name) {
		this.name = name;
		win = 0;
		lose = 0;
		draw = 0;
	}

	public void setName(char name) {
		this.name = name;

	}

	public char getName() {
		return name;
	}

	public void win() {
		win = win +1;
	}

	public void lose() {
		lose = lose +1;
	}

	public void draw() {
		draw = draw +1;
	}

	public int getWin() {
		return win;
	}

	public int getLose() {
		return lose;
	}

	public int getDraw() {
		return draw;
	}

}
