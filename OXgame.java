package myXOgame;

import java.util.InputMismatchException;
import java.util.Scanner;

public class OXgame {

	private Table table;
	private Player O;
	private Player X;
	private int row;
	private int col;
	private char again = 'Y';
	Scanner ip;

	public void startGame() {
		this.O = new Player('O');
		this.X = new Player('X');
		table = new Table(O, X);

	}

	public void showWelcome() {
		System.out.println("******Welcome to XO game******");
	}

	public void showTable() {
		char[][] data = table.getTableOX();
		for (int i = 0; i < data.length; i++) {
			System.out.print("| ");
			for (int j = 0; j < data.length; j++) {
				System.out.print(data[i][j] + " | ");
			}
			System.out.println();
		}

	}

	public void showTurn() {
		System.out.println("turn " + table.getPlayer().getName());
	}

	public void inputRowCol() {
		ip = new Scanner(System.in);
		System.out.print("Please input row and column : ");
		try {
			int row = ip.nextInt();
			int col = ip.nextInt();
			if (table.checkError(row, col) == true) {
				inputRowCol();
			} else {
				table.inputTable(row, col);
			}
		} catch (InputMismatchException e) {
			System.out.println("Input mismatch , try again");
		}

	}

	public void showScore() {
		System.out.println("X Win: " + this.X.getWin() + " Lose: " + this.X.getLose() + " Draw: " + this.X.getDraw());
		System.out.println("O Win: " +this.O.getWin() + " Lose: " + this.O.getLose() + " Draw: " + this.O.getDraw());
	}

	public void showWinner() {
		System.out.println(table.getWinner() + " Win");
	}

	public void showBye() {
		System.out.println("Good bye");
	}

	public boolean askPlayAgain() {
		System.out.print("You want to play agian?(Y/N) : ");
		again = ip.next().charAt(0);
		if (again == 'Y') {
			return true;
		} else if (again == 'N') {
			return false;
		}
		return false;
	}

	public void play() {
		showWelcome();
		while (again == 'Y') {
			showTable();
			showTurn();
			inputRowCol();

			if (table.checkWin() || table.checkDraw()) {
				showTable();
				table.scoreUpdate();
				showWinner();
				showScore();
				askPlayAgain();
				table.clearTable();
				if (again == 'N') {
					showBye();
					break;
				}

			}
		
		}
		
	}

}
