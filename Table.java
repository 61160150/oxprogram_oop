package myXOgame;

public class Table {

	private char[][] tableOX = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	private Player first;
	private Player second;
	char winner;
	private Player player;
	private int countturn = 0;
	private Player O;
	private Player X;

	public Table(Player first, Player second) {
		this.first = first;
		this.second = second;
		this.player = first;
		this.O = new Player('O');
		this.X = new Player('X');
	}

	public void inputTable(int row, int col) {
		tableOX[row - 1][col - 1] = player.getName();
		countturn++;
		checkWin();
		if (checkWin() == false) {
			switchPlayer();
		}
	}

	public char[][] getTableOX() {
		return tableOX;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Player getPlayer() {
		return player;
	}


	public void switchPlayer() {
		if (player.getName() == first.getName()) {
			player = second;
		} else if (player.getName() == second.getName()) {
			player = first;
		}
	}

	public boolean checkWin() {
		if (checkWinRow()) {
			winner = this.player.getName();
			return true;
		} else if (checkWinCol()) {
			winner = this.player.getName();
			return true;
		} else if (checkWinX()) {
			winner = this.player.getName();
			return true;
		}
		return false;
	}

	public boolean checkDraw() {
		if (countturn == 9 && checkWin() == false) {
			return true;
		}
		return false;
	}

	public boolean checkWinRow() {
		if (tableOX[0][0] == player.getName() && tableOX[0][1] == player.getName()
				&& tableOX[0][2] == player.getName()) {
			return true;
		} else if (tableOX[1][0] == player.getName() && tableOX[1][1] == player.getName()
				&& tableOX[1][2] == player.getName()) {
			return true;
		} else if (tableOX[2][0] == player.getName() && tableOX[2][1] == player.getName()
				&& tableOX[2][2] == player.getName()) {
			return true;
		}

		return false;
	}

	public boolean checkWinCol() {
		if (tableOX[0][0] == player.getName() && tableOX[1][0] == player.getName()
				&& tableOX[2][0] == player.getName()) {
			return true;
		} else if (tableOX[0][1] == player.getName() && tableOX[1][1] == player.getName()
				&& tableOX[2][1] == player.getName()) {
			return true;
		} else if (tableOX[0][2] == player.getName() && tableOX[1][2] == player.getName()
				&& tableOX[2][2] == player.getName()) {
			return true;
		}

		return false;
	}

	public boolean checkWinX() {
		if (checkWinX1()) {
			return true;
		} else if (checkWinX2()) {
			return true;
		}
		return false;
	}

	public boolean checkWinX1() {
		for (int i = 0; i < tableOX.length; i++) {
			if (tableOX[i][i] != player.getName()) {
				return false;
			}
		}
		return true;
	}

	public boolean checkWinX2() {
		for (int i = 0; i < tableOX.length; i++) {
			if (tableOX[i][2 - i] != player.getName()) {
				return false;
			}
		}
		return true;
	}

	public boolean checkError(int row, int col) {
		if (row > 3 || row < 1 || col > 3 || col < 1) {
			System.out.println("Error!! Row : Please input 1 or 2 or 3");
			return true;
		} else if (tableOX[row - 1][col - 1] == 'O' || tableOX[row - 1][col - 1] == 'X') {
			System.out.println("You can't select this location " + "because it has already been chosen.");
			return true;
		}
		return false;
	}

	public char getWinner() {
		return winner;
	}

	public  void scoreUpdate() {
		if (winner== O.getName()) {
			O.win();
			X.lose();
		} else if (winner == X.getName()) {
			X.win();
			O.lose();
		} else if (checkDraw()) {
			X.draw();
			O.draw();
		}
	}

	public void clearTable() {
		for (int row = 0; row < tableOX.length; row++) {
			for (int col = 0; col < tableOX.length; col++) {
				tableOX[row][col] = '-';
			}

		}

	}
}
